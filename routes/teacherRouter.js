// router.js
const router = require("express").Router();

// Controllers
const teacher = require("../controllers/teacherController");
const middleware = require("../middlewares/middlewares");


router.get("/register", (req, res) => {
  res.render("register_teacher")
});
router.post("/register", teacher.register);

router.get("/login", teacher.getLoginPage);
router.post("/login", teacher.login);

// daerah wajib penggunaan middlewares
router.get("/dashboard", middleware.restrictTeacher, teacher.dashboard);
router.get('/profil', middleware.restrictTeacher, teacher.edit);

router.get('/all-teacher', middleware.restrictTeacher, teacher.allTeacher);
router.get('/logout', middleware.restrictTeacher, teacher.logout);

router.get('/:id', middleware.restrictTeacher, teacher.detail);
router.put('/:id', middleware.restrictTeacher, teacher.update);
router.delete('/:id', middleware.restrictTeacher, teacher.delete);


module.exports = router;